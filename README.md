**Pet adoption app**

This is a mobile app project developed using flutter framework and was designed by a random artist. This application uses a beautiful UI and minimal animation to create a friendly user experience.

This application is oriented to be used as a system that helps people adopt pet animals with the porpouse of helping them from suffering on the streets and creating a culture of adopting animals instead of buying them.

**TODO**

- [ ] Implement backend server.
- [ ] Implement state management ( I recommend using riverpod).
- [ ] Add login and registration forms.


**Screenshots**
- Home Screen.
<img src="/screenshots/home_page.png" alt="Home screenshot" width="200"/>
<br>

- Pet Details Screen.
<img src="/screenshots/pet_details.png" alt="Pet Details page screenshot" width="200"/>