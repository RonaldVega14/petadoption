import 'package:flutter/material.dart';

import 'configuration.dart';

Widget appBar() => Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(icon: Icon(Icons.menu), onPressed: null),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Location',
                textAlign: TextAlign.center,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 4.0),
                    child: Icon(
                      Icons.location_on,
                      color: primaryGreen,
                    ),
                  ),
                  Text(
                    'Ukraine',
                  ),
                ],
              ),
            ],
          ),
          CircleAvatar(),
        ],
      ),
    );
