import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:pet_adoption/utils/configuration.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: primaryGreen,
      padding: EdgeInsets.only(top: 40, bottom: 25.0, left: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              CircleAvatar(),
              Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: Column(
                  children: [
                    Text(
                      'Ronald Vega',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Circular'),
                    ),
                    Text(
                      'Active Status',
                      style: TextStyle(
                          color: Colors.white70, fontFamily: 'Circular'),
                    )
                  ],
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 4.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: drawerItems
                  .map((element) => Padding(
                        padding: const EdgeInsets.symmetric(vertical: 12.0),
                        child: Row(
                          children: [
                            Icon(element['icon'],
                                color: Colors.white, size: 30),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              element['title'],
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  fontFamily: 'Circular'),
                            )
                          ],
                        ),
                      ))
                  .toList(),
            ),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.settings,
                  color: Colors.white70,
                ),
              ),
              Text(
                'Settings',
                style: TextStyle(color: Colors.white70, fontFamily: 'Circular'),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                width: 2,
                height: 20,
                color: Colors.white70,
              ),
              Text(
                'Log out',
                style: TextStyle(color: Colors.white70, fontFamily: 'Circular'),
              )
            ],
          ),
        ],
      ),
    );
  }
}
