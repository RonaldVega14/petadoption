import 'package:flutter/material.dart';
import 'package:pet_adoption/utils/configuration.dart';

class PetDetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
              child: Column(
            children: [
              Expanded(
                flex: 4,
                child: Container(
                  color: Colors.blueGrey[300],
                ),
              ),
              Expanded(
                flex: 5,
                child: Container(
                  color: Colors.white,
                ),
              )
            ],
          )),
          Container(
            margin: EdgeInsets.only(top: 30, left: 20.0, right: 20.0),
            child: Align(
              alignment: Alignment.topCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      icon: Icon(Icons.arrow_back_ios),
                      onPressed: () => Navigator.pop(context)),
                  IconButton(icon: Icon(Icons.share), onPressed: null)
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child:
                Hero(tag: 1, child: Image.asset('assets/images/pet-cat2.png')),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 80.0),
            child: Align(
              alignment: Alignment.center,
              child: Hero(
                tag: '1-container',
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 30),
                  height: 130,
                  decoration: BoxDecoration(
                      boxShadow: shadowList,
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15.0, vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Saola',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontFamily: 'Circular',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black87),
                              ),
                              Icon(Icons.favorite_border_outlined,
                                  color: Colors.grey)
                            ],
                          ),
                        ),
                        Expanded(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Abyssinian cat',
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'Circular',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[600]),
                            ),
                            Text(
                              '2 years old',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: 'Circular',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey[600]),
                            ),
                          ],
                        )),
                        Expanded(
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 4.0),
                                child: Icon(
                                  Icons.location_on,
                                  color: primaryGreen,
                                ),
                              ),
                              Text(
                                'Distance: 3.6 km',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontFamily: 'Circular',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey[400]),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.only(bottom: 150, left: 10, right: 10),
              height: 160,
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                            flex: 3,
                            child: Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(right: 6.0),
                                  padding: const EdgeInsets.all(4.0),
                                  child: CircleAvatar(
                                    backgroundImage:
                                        AssetImage('assets/images/avatar.jpg'),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 3.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Alejandro Vega',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: 'Circular',
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black87),
                                        textAlign: TextAlign.start,
                                      ),
                                      Text(
                                        'Owner',
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontFamily: 'Circular',
                                            fontWeight: FontWeight.bold,
                                            color: Colors.grey[400]),
                                        textAlign: TextAlign.start,
                                      )
                                    ],
                                  ),
                                )
                              ],
                            )),
                        Flexible(
                            child: Text(
                          'May 25, 2019',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: 'Circular',
                              fontWeight: FontWeight.bold,
                              color: Colors.grey[400]),
                        ))
                      ],
                    ),
                    flex: 1,
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 12.0),
                      child: Text(
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam',
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: 'Circular',
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[400],
                            wordSpacing: 1.4,
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 120,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
                child: Row(
                  children: [
                    Container(
                      height: 60,
                      width: 70,
                      decoration: BoxDecoration(
                          color: primaryGreen,
                          borderRadius: BorderRadius.circular(20)),
                      child: Icon(
                        Icons.favorite_border,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                            color: primaryGreen,
                            borderRadius: BorderRadius.circular(20)),
                        child: Center(
                            child: Text(
                          'Adoption',
                          style: TextStyle(
                              letterSpacing: 1.2,
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Circular'),
                        )),
                      ),
                    )
                  ],
                ),
              ),
              decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  )),
            ),
          )
        ],
      ),
    );
  }
}
