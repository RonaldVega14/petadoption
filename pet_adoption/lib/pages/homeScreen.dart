import 'package:flutter/material.dart';
import 'package:pet_adoption/pages/petDetailsScreen.dart';
import 'package:pet_adoption/utils/configuration.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;

  bool isDrawerOpen = false;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      transform: Matrix4.translationValues(xOffset, yOffset, 0)
        ..scale(scaleFactor)
        ..rotateY(isDrawerOpen ? -0.5 : 0),
      decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.circular(isDrawerOpen ? 40 : 0)),
      duration: Duration(milliseconds: 250),
      child: SingleChildScrollView(
        child: Column(
          children: [
            //Top Margin
            SizedBox(
              height: 35.0,
            ),
            //Appbar
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  isDrawerOpen
                      ? IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          onPressed: () {
                            setState(() {
                              xOffset = 0;
                              yOffset = 0;
                              scaleFactor = 1;
                              isDrawerOpen = false;
                            });
                          })
                      : IconButton(
                          icon: Icon(Icons.menu),
                          onPressed: () {
                            setState(() {
                              xOffset = 230;
                              yOffset = 150;
                              scaleFactor = 0.65;
                              isDrawerOpen = true;
                            });
                          }),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Location',
                        style: TextStyle(fontFamily: 'Circular'),
                        textAlign: TextAlign.center,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 4.0),
                            child: Icon(
                              Icons.location_on,
                              color: primaryGreen,
                            ),
                          ),
                          Text(
                            'Kyiv, Ukraine',
                            style: TextStyle(
                                fontFamily: 'Circular',
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ],
                  ),
                  CircleAvatar(
                    backgroundImage: AssetImage('assets/images/avatar.jpg'),
                  ),
                ],
              ),
            ),

            //Search bar
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(Icons.search),
                  Text(
                    'Search pet to adopt',
                    style: TextStyle(fontFamily: 'Circular'),
                  ),
                  Icon(Icons.settings)
                ],
              ),
            ),

            //Categories bar
            Container(
                height: 95,
                padding: EdgeInsets.only(left: 10.0),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: categories.length,
                  itemBuilder: (context, index) {
                    return Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.only(left: 10, right: 10.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: shadowList,
                                borderRadius: BorderRadius.circular(10)),
                            child: Image.asset(
                              categories[index]['iconPath'],
                              height: 50,
                              width: 50,
                              color: Colors.grey[700],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 6.0),
                            child: Text(
                              categories[index]['name'],
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    );
                  },
                )),

            GestureDetector(
              onTap: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PetDetailsScreen())),
              child: Container(
                height: 240,
                margin: EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  children: [
                    Expanded(
                        child: Stack(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.blueGrey[300],
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: shadowList,
                          ),
                          margin: EdgeInsets.only(top: 50),
                        ),
                        Align(
                          child: Hero(
                              tag: 1,
                              child: Image.asset('assets/images/pet-cat2.png')),
                        )
                      ],
                    )),
                    Expanded(
                        child: Hero(
                      tag: '1-container',
                      child: Container(
                        margin: EdgeInsets.only(top: 70, bottom: 20),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: shadowList,
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(20),
                                bottomRight: Radius.circular(20))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 15.0, vertical: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Saola',
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontFamily: 'Circular',
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black87),
                                    ),
                                    Icon(Icons.favorite_border_outlined,
                                        color: Colors.grey)
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Text(
                                  'Abyssinian cat',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: 'Circular',
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[600]),
                                ),
                              ),
                              Expanded(
                                child: Text(
                                  '2 years old',
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: 'Circular',
                                      color: Colors.grey[700]),
                                ),
                              ),
                              Expanded(
                                child: Row(
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 4.0),
                                      child: Icon(
                                        Icons.location_on,
                                        color: primaryGreen,
                                      ),
                                    ),
                                    Text(
                                      'Distance: 3.6 km',
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: 'Circular',
                                          color: Colors.grey[700]),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ))
                  ],
                ),
              ),
            ),
            Container(
              height: 240,
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: [
                  Expanded(
                    child: Stack(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.orange[100],
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: shadowList,
                          ),
                          margin: EdgeInsets.only(top: 50),
                        ),
                        Align(
                          child: Hero(
                              tag: 2,
                              child: Image.asset('assets/images/pet-cat1.png')),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                      child: Hero(
                    tag: '2-container',
                    child: Container(
                      margin: EdgeInsets.only(top: 60, bottom: 20),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: shadowList,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20))),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15.0, vertical: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Orion',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontFamily: 'Circular',
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black87),
                                  ),
                                  Icon(Icons.favorite_border_outlined,
                                      color: Colors.grey)
                                ],
                              ),
                            ),
                            Expanded(
                              child: Text(
                                'Abyssinian cat',
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'Circular',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey[600]),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                '1.5 years old',
                                style: TextStyle(
                                    fontSize: 14,
                                    fontFamily: 'Circular',
                                    color: Colors.grey[700]),
                              ),
                            ),
                            Expanded(
                              child: Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(right: 4.0),
                                    child: Icon(
                                      Icons.location_on,
                                      color: primaryGreen,
                                    ),
                                  ),
                                  Text(
                                    'Distance: 7.8 km',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: 'Circular',
                                        color: Colors.grey[700]),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ))
                ],
              ),
            ),
            SizedBox(
              height: 50,
            )
          ],
        ),
      ),
    );
  }
}
